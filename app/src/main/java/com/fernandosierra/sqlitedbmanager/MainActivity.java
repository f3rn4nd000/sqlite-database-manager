package com.fernandosierra.sqlitedbmanager;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.fernandosierra.sqlitedbmanager.core.BaseResult;
import com.fernandosierra.sqlitedbmanager.core.DatabaseManager;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = ((TextView) findViewById(R.id.text));
        findViewById(R.id.btn_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseManager.getInstance().appendOperation(new InsertUserOperation());
            }
        });
        findViewById(R.id.btn_get).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseManager.getInstance().appendOperation(new RetrieveUsersOperation());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        DatabaseManager.getInstance().registerReceiver(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        DatabaseManager.getInstance().unregisterReceiver(this);
    }

    public void onEvent(BaseResult event) {
        final String text;
        if (event instanceof RetrieveUsersResult) {
            RetrieveUsersResult result = ((RetrieveUsersResult) event);
            if (result.getStatus() == BaseResult.OK) {
                final List<User> users = result.getUsers();
                if (!users.isEmpty()) {
                    text = users.get(users.size() - 1).toString();
                } else {
                    text = "Nothing to show";
                }
            } else {
                text = result.getException().getLocalizedMessage();
            }
        } else {
            InsertUserResult result = ((InsertUserResult) event);
            if (result.getStatus() == BaseResult.OK) {
                long id = result.getId();
                text = "Inserted id: " + String.valueOf(id);
            } else {
                text = result.getException().getLocalizedMessage();
            }
        }
        Handler handler = new Handler(getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                textView.setText(text);
            }
        });
    }
}