package com.fernandosierra.sqlitedbmanager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/11/15.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE = "Database.sqlite";
    public static final int VERSION = 1;
    public static final String CREATE_USER =
        "CREATE TABLE IF NOT EXISTS User(_id INTEGER PRIMARY KEY, name TEXT, username TEXT, password" + " TEXT)";

    public DatabaseHelper(Context context) {
        super(context, DATABASE, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Nothing
    }
}
