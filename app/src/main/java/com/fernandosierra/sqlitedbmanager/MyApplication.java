package com.fernandosierra.sqlitedbmanager;

import android.app.Application;

import com.fernandosierra.sqlitedbmanager.core.DatabaseManager;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/11/15.
 */
public class MyApplication extends Application {

    private DatabaseManager databaseManager;

    @Override
    public void onCreate() {
        super.onCreate();
        databaseManager = DatabaseManager.getNewInstance(new DatabaseHelper(this));
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        databaseManager.stop();
    }
}
