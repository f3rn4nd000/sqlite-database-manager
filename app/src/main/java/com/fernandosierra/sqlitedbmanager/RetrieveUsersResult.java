package com.fernandosierra.sqlitedbmanager;

import android.database.sqlite.SQLiteException;

import com.fernandosierra.sqlitedbmanager.core.BaseResult;

import java.util.List;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/11/15.
 */
public class RetrieveUsersResult extends BaseResult {

    private List<User> users;

    public RetrieveUsersResult(@Status int status, SQLiteException exception) {
        super(status, exception);
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
