package com.fernandosierra.sqlitedbmanager;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.NonNull;

import com.fernandosierra.sqlitedbmanager.core.BaseOperation;
import com.fernandosierra.sqlitedbmanager.core.BaseResult;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/11/15.
 */
public class RetrieveUsersOperation extends BaseOperation<RetrieveUsersResult> {

    @Override
    public int getPriority() {
        return NORMAL;
    }
    @Override
    public RetrieveUsersResult performOperation(@NonNull SQLiteDatabase database) {
        RetrieveUsersResult result = new RetrieveUsersResult(BaseResult.OK, null);
        try {
            List<User> users = new ArrayList<>();
            Cursor cursor = database.query("User", null, null, null, null, null, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    User user = new User();
                    user.setId(cursor.getLong(cursor.getColumnIndexOrThrow("_id")));
                    user.setName(cursor.getString(cursor.getColumnIndexOrThrow("name")));
                    user.setUsername(cursor.getString(cursor.getColumnIndexOrThrow("username")));
                    user.setPassword(cursor.getString(cursor.getColumnIndexOrThrow("password")));
                    users.add(user);
                }
                cursor.close();
            }
            result.setUsers(users);
            return result;
        } catch (SQLiteException | IllegalArgumentException e) {
            result.setStatus(BaseResult.FAIL);
            result.setException(e);
            return result;
        }
    }
}
