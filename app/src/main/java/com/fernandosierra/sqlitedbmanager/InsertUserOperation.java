package com.fernandosierra.sqlitedbmanager;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.NonNull;
import android.util.Log;

import com.fernandosierra.sqlitedbmanager.core.BaseOperation;
import com.fernandosierra.sqlitedbmanager.core.BaseResult;
import com.fernandosierra.sqlitedbmanager.core.DatabaseManager;

import java.util.Random;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/11/15.
 */
public class InsertUserOperation extends BaseOperation<InsertUserResult> {

    @Override
    public InsertUserResult performOperation(@NonNull SQLiteDatabase database) {
        try {
            ContentValues contentValues = new ContentValues();
            Random random = new Random(System.currentTimeMillis());
            int value = random.nextInt();
            contentValues.put("name", "Fernando Sierra");
            contentValues.put("username", "fer" + value);
            contentValues.put("password", "qwerty" + value);
            long id = database.insert("User", null, contentValues);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Log.e(DatabaseManager.TAG, e.getLocalizedMessage(), e);
            }
            InsertUserResult insertUserResult = new InsertUserResult(BaseResult.OK, null);
            insertUserResult.setId(id);
            return insertUserResult;
        } catch (SQLiteException e) {
            return new InsertUserResult(BaseResult.FAIL, e);
        }
    }

    @Override
    public int getPriority() {
        return HIGH;
    }
}
