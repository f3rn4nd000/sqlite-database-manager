package com.fernandosierra.sqlitedbmanager;

import android.database.sqlite.SQLiteException;

import com.fernandosierra.sqlitedbmanager.core.BaseResult;

/**
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/11/15.
 */
public class InsertUserResult extends BaseResult {

    private long id;

    public InsertUserResult(@Status int status, SQLiteException exception) {
        super(status, exception);
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
}
