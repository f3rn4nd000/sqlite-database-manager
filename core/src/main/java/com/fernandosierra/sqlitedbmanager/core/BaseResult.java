package com.fernandosierra.sqlitedbmanager.core;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.IntDef;

/**
 * Class to wrap whole the information about how ended the operation in the {@link DatabaseManager}.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/11/15.
 */
public class BaseResult {

    public static final int OK = 1;
    public static final int FAIL = 0;

    @IntDef({OK, FAIL})
    public @interface Status {}

    private
    @BaseResult.Status
    int status;
    private Exception exception;

    /**
     * Constructor
     *
     * @param status
     *     Status to be attached to the result.
     * @param exception
     *     Exception that occurs during the {@link BaseOperation#performOperation(SQLiteDatabase)} method.
     */
    public BaseResult(@Status int status, Exception exception) {
        this.status = status;
        this.exception = exception;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
