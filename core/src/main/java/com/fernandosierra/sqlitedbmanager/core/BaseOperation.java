package com.fernandosierra.sqlitedbmanager.core;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;

/**
 * Class to provides the basic mechanisms to perform operations and gives to the {@link DatabaseManager} information about the operation's
 * priority to be performed.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/11/15.
 */
public abstract class BaseOperation<T extends BaseResult> implements Comparable<BaseOperation> {

    public static final int LOW = 1;
    public static final int NORMAL = 2;
    public static final int HIGH = 3;
    public static final int MAXIMUM = 4;

    @IntDef({LOW, NORMAL, HIGH, MAXIMUM})
    public @interface Priority {}

    /**
     * Retrieves the priority of the operation. With this value the {@link DatabaseManager} can put the operation in the best place in the
     * queue.
     *
     * @return The priority level of the operation.
     */
    public abstract
    @Priority
    int getPriority();

    /**
     * This method will contains all the database operations to be performed inside the {@link DatabaseManager}.
     *
     * @param database
     *     Database where will performed the operation.
     */
    public abstract T performOperation(@NonNull SQLiteDatabase database);

    @Override
    public int compareTo(@NonNull BaseOperation another) {
        int priority = getPriority();
        int anotherPriority = another.getPriority();
        if (priority < anotherPriority) {
            return -1;
        } else if (priority > anotherPriority) {
            return 1;
        } else {
            return 0;
        }
    }
}
