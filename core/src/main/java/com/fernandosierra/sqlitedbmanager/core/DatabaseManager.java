package com.fernandosierra.sqlitedbmanager.core;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.concurrent.PriorityBlockingQueue;

import de.greenrobot.event.EventBus;

/**
 * This class provides a centralized asynchronous management, worker, and notification system for a SQLite database into an Android
 * Application.
 *
 * @author Ing. Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/11/15.
 */
public class DatabaseManager {

    public static final String TAG = "DatabaseManager";
    private static DatabaseManager instance;
    private SQLiteOpenHelper sqLiteOpenHelper;
    private PriorityBlockingQueue<BaseOperation> queue = new PriorityBlockingQueue<>();
    private EventBus eventBus;
    private Thread thread;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            // Get and block the database, to prevent the access for another thread.
            SQLiteDatabase database = sqLiteOpenHelper.getWritableDatabase();
            while (!queue.isEmpty()) {
                // Get and extract the next work in the queue.
                BaseOperation operation = queue.poll();
                // Make whatever the operation needs to do and notify the event to
                // everybody is listening the event.
                BaseResult result = operation.performOperation(database);
                eventBus.post(result);
            }
            // We release the database when we are done with the pending jobs.
            sqLiteOpenHelper.close();
        }
    };

    /**
     * Retrieves a instance to the {@link DatabaseManager}. Always be returned the same instance and never will be null.
     *
     * @param sqLiteOpenHelper
     *     The helper to be attached to the manager, and will provide whole the information about the database.
     * @return A new/same instance for the manager. Never will be null.
     */
    @NonNull
    public static DatabaseManager getNewInstance(SQLiteOpenHelper sqLiteOpenHelper) {
        if (instance == null) {
            instance = new DatabaseManager(sqLiteOpenHelper);
        }
        return instance;
    }

    /**
     * Retrieves a instance to the {@link DatabaseManager}. Always be returned the same instance and never will be null (You MUST call
     * before the {@link #getNewInstance(SQLiteOpenHelper)} method to provide the information of the database to be attached).
     *
     * @return An instance for the manager. Never will be null.
     */
    @NonNull
    public static DatabaseManager getInstance() {
        return instance;
    }

    /**
     * Constructor
     *
     * @param sqLiteOpenHelper
     *     Helper to provide all the information about the database to will be attached.
     */
    private DatabaseManager(@NonNull SQLiteOpenHelper sqLiteOpenHelper) {
        this.sqLiteOpenHelper = sqLiteOpenHelper;
        eventBus = EventBus.getDefault();
    }

    /**
     * Stop the thread if it is are running and when it is possible.
     */
    public void stop() {
        if (thread.isAlive()) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                Log.e(TAG, e.getLocalizedMessage(), e);
            }
        }
    }

    /**
     * Registers a receiver to the events triggered when a are dispatched for the manager. You would call it on the {@link
     * Activity#onResume()} method.
     *
     * @param subscriber
     *     The object to be receive the notifications about the operation recently dispatched. The receiver MUST have an onEvent(Object
     *     event) method where will be send it the event.
     */
    public void registerReceiver(@NonNull Object subscriber) {
        eventBus.register(subscriber);
        Log.i(TAG, subscriber.toString() + " registered");
    }

    /**
     * Unregister a previous receiver of notifications abouts the operations dispatched. You would call it on the {@link Activity#onPause()}
     * method.
     *
     * @param subscriber
     *     The object to stops receiving the notifications about the operation recently dispatched. The receiver MUST have an onEvent(Object
     *     event) method where will be send it the event.
     */
    public void unregisterReceiver(@NonNull Object subscriber) {
        eventBus.unregister(subscriber);
        Log.i(TAG, subscriber.toString() + " unregistered");
    }

    /**
     * Append a new operation to the queue, the place in the queue will depend of the {@link com.fernandosierra.sqlitedbmanager.core
     * .BaseOperation.Priority} of the operation. And if it is necessary start to run the manager.
     *
     * @param operation
     *     Operation to be performed when it turn comes.
     */
    public void appendOperation(@NonNull BaseOperation operation) {
        queue.add(operation);
        if (thread == null || thread.getState() == Thread.State.TERMINATED) {
            thread = new Thread(runnable);
            thread.start();
        }
    }

    /**
     * Remove a previously appended operation from the queue.
     *
     * @param operation
     *     Operation to be removed from the queue if it is possible.
     */
    public void removeOperation(@NonNull BaseOperation operation) {
        queue.remove(operation);
    }
}
